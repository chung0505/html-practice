let countH = 0
let countG = 0
let homeEl = document.getElementById("home-score-el")
let guestEl = document.getElementById("guest-score-el")

function addOneHomeTeam() {
    countH += 1
    homeEl.textContent = countH
}

function addOneGuestTeam() {
    countG += 1
    guestEl.textContent = countG
}

function addTwoHomeTeam() {
    countH += 2
    homeEl.textContent = countH
}

function addTwoGuestTeam() {
    countG += 2
    guestEl.textContent = countG
}

function addThreeHomeTeam() {
    countH += 3
    homeEl.textContent = countH
}

function addThreeGuestTeam() {
    countG += 3
    guestEl.textContent = countG
}
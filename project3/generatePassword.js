const characters = ["A","B","C","D","E","F","G","H","I","J","K","L","M","N","O","P","Q","R","S","T","U","V","W","X","Y","Z","a","b","c","d","e","f","g","h","i","j","k","l","m","n","o","p","q","r","s","t","u","v","w","x","y","z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9","~","`","!","@","#","$","%","^","&","*","(",")","_","-","+","=","{","[","}","]",",","|",":",";","<",">",".","?",
"/"];

function generate() {
    let firstPassword = document.getElementById("first-box-el")
    let secondPassword = document.getElementById("second-box-el")
    let number = document.getElementById("number-of-password").value
    console.log(number)
    let password1 = ""
    let password2 = ""
    for(let i = 0; i < number; i++) {
        password1 += characters[Math.floor(Math.random() * characters.length)]
        password2 += characters[Math.floor(Math.random() * characters.length)]
    }
    firstPassword.textContent = password1
    secondPassword.textContent = password2
}